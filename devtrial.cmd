@echo off
SETLOCAL ENABLEDELAYEDEXPANSION

set systempass=SystemUserH2O
set dbsid=somedbid
set trialtype=DEV
set deploywsport=14040
set trialapproval=FALSE
set backuppath=E:\DeployBackup
set name=%2
call :UCase name upname

if "%1" == "setup" goto setup
if "%1" == "kill" goto kill
if "%1" == "reboot" goto reboot
if "%1" == "restart" goto restart
if "%1" == "help" goto help
if "%1" == "view" goto view
goto invalid

:setup
	if "%2" == "" goto noname
	
	:: Set up server and trial
	echo.
	echo Setting up new trial...
	echo ---------------------------------------------------------------------------
	pfadmin setup server %name%
	echo uid=dba%name%> trialdbparams.txt
	echo pid=dba%name%>> trialdbparams.txt
	pfadmin setup trial %name% %name% /DB %dbsid% /accountparams:"trialdbparams.txt"
	del trialdbparams.txt
	pfadmin CONFIG WEBSERVICE %name% DeploymentService ADD HTTP:%deploywsport%
	
	:: Set up database and import MedML files if they exist in the current directory
	echo.
	echo Setting up database...
	echo ---------------------------------------------------------------------------
	if exist %CD%\Admin.rsp (dbsetup %name% %CD%\Admin.rsp) else (dbsetup %name% C:\InFormScripts\Admin.rsp)
	pfadmin start server %name%
	pfadmin start trial %name%

	:: Trial configuration
	echo.
	echo Configuring trial...
	echo ---------------------------------------------------------------------------
	pfadmin config trial %name% /TrialType %trialtype%
	pfadmin config trial %name% /TrialApproval %trialapproval%
	pfadmin config trial %name% /DeployBackupFolder %backuppath%

	:: Set system user password
	echo pid=%systempass%> systempw.txt
	pfadmin setserver systempw %name% /accountparams:"systempw.txt"
	del systempw.txt
	
	echo.
	echo ---------------------------------------------------------------------------
	echo Trial setup complete.
	echo ---------------------------------------------------------------------------
	URIAdd %name%
	exit /b

:kill
	if "%2" == "" goto noname
	
	echo.
	echo Removing trial %name%...
	echo ---------------------------------------------------------------------------
	pfadmin stop trial %name%
	pfadmin stop server %name%
	pfadmin remove trial %name% /DSN
	pfadmin remove server %name%
	echo.
	echo Dropping database user...
	echo ---------------------------------------------------------------------------
	sqlplus pfdbadmin/pfdbadmin@%dbsid% @C:\InFormScripts\kill.sql DBA%upname%
	echo.
	echo ---------------------------------------------------------------------------
	echo Trial removed.
	echo ---------------------------------------------------------------------------
	URIDelete %name%
	exit /b

:reboot
	if "%2" == "" goto noname

	:: Remove trial
	echo.
	echo Removing trial %name%...
	echo ---------------------------------------------------------------------------
	pfadmin stop trial %name%
	pfadmin stop server %name%
	pfadmin remove trial %name% /DSN
	pfadmin remove server %name%
	echo.
	echo Dropping database user...
	echo ---------------------------------------------------------------------------
	sqlplus pfdbadmin/pfdbadmin@%dbsid% @C:\InFormScripts\kill.sql DBA%upname%
	echo.
	echo Trial removed.
	timeout 10

	:: Set up server and trial
	echo.
	echo Setting up new trial...
	echo ---------------------------------------------------------------------------
	pfadmin setup server %name%
	echo uid=dba%name%> trialdbparams.txt
	echo pid=dba%name%>> trialdbparams.txt
	pfadmin setup trial %name% %name% /DB %dbsid% /accountparams:"trialdbparams.txt"
	del trialdbparams.txt
	pfadmin CONFIG WEBSERVICE %name% DeploymentService ADD HTTP:%deploywsport%

	:: Set up database and import MedML files if they exist in the current directory
	echo.
	echo Setting up database...
	echo ---------------------------------------------------------------------------
	if exist %CD%\Admin.rsp (dbsetup %name% %CD%\Admin.rsp) else (dbsetup %name% C:\InFormScripts\Admin.rsp)
	pfadmin start server %name%
	pfadmin start trial %name%

	:: Trial configuration
	echo.
	echo Configuring trial...
	echo ---------------------------------------------------------------------------
	pfadmin config trial %name% /TrialType %trialtype%
	pfadmin config trial %name% /TrialApproval %trialapproval%
	pfadmin config trial %name% /DeployBackupFolder %backuppath%

	:: Set system user password
	echo pid=%systempass%> systempw.txt
	pfadmin setserver systempw %name% /accountparams:"systempw.txt"
	del systempw.txt
	echo.
	echo ---------------------------------------------------------------------------
	echo Trial reboot complete.
	echo ---------------------------------------------------------------------------
	exit /b
	
:restart
	if "%2" == "" goto noname

	echo.
	echo Restarting trial...
	echo ---------------------------------------------------------------------------
	pfadmin stop trial %name%
	pfadmin stop server %name%
	timeout 10
	pfadmin start server %name%
	pfadmin start trial %name%
	echo.
	echo ---------------------------------------------------------------------------
	echo Trial restart complete.
	echo ---------------------------------------------------------------------------
	exit /b

:view
	pfadmin view service
	exit /b

:help
	echo.
	echo ---------------------------------------------------------------------------
	echo Welcome to the H2O Clinical InForm trial builder!
	echo ---------------------------------------------------------------------------
	echo.
	echo Command syntax is:
	echo devtrial [command] [trialname]
	echo.
	echo Commands available are:
	echo setup   ---  Setup a brand new development trial.
	echo kill    ---  Kill an existing trial, including dropping the database schema.
	echo reboot  ---  Reboot an existing trial by dropping and creating a new trial.
	echo restart ---  Restart a trial in case of hiccups.
	echo view    ---  View all existing trials.
	echo.
	echo ---------------------------------------------------------------------------
	exit /b

:noname
	echo No trial name given. Type "devtrial help" for more information.
	exit /b	
	
:invalid
	if "%1" == "" (
		echo No command passed. Type "devtrial help" for more information.
		exit /b
	)
	echo Invalid argument. Type "devtrial help" for more information.
	exit /b
	

:: Credit to Brad Thone http://www.robvanderwoude.com/battech_convertcase.php
:LCase
:UCase
:: Converts to upper/lower case variable contents
:: Syntax: CALL :UCase _VAR1 _VAR2
:: Syntax: CALL :LCase _VAR1 _VAR2
:: _VAR1 = Variable NAME whose VALUE is to be converted to upper/lower case
:: _VAR2 = NAME of variable to hold the converted value
:: Note: Use variable NAMES in the CALL, not values (pass "by reference")

SET _UCase=A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
SET _LCase=a b c d e f g h i j k l m n o p q r s t u v w x y z
SET _Lib_UCase_Tmp=!%1!
IF /I "%0"==":UCase" SET _Abet=%_UCase%
IF /I "%0"==":LCase" SET _Abet=%_LCase%
FOR %%Z IN (%_Abet%) DO SET _Lib_UCase_Tmp=!_Lib_UCase_Tmp:%%Z=%%Z!
SET %2=%_Lib_UCase_Tmp%
GOTO:EOF