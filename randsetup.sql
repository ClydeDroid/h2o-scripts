set serveroutput on size 20000
set pause 		off
set timing		off
set verify 		off
set echo 		off
set feedback	off
set heading 	off

undefine randuser
undefine randPassword
undefine dbinstance
undefine syspass
undefine inftbl
undefine inftemptbl
undefine informuid
undefine logfilename

define randuser = 'rand'
define randPassword = 'rand'
define dbinstance = 'somedbid'
define inftbl = 'INFORM'
define inftemptbl = 'TEMPBIG'
define informuid = 'dba'
define logfilename = 'randsetup.log'

spool &&logfilename

prompt > Creating randmization database...

connect pfdbadmin/pfdbadmin@&&dbinstance

Drop user &&randuser CASCADE;

CREATE USER &&randuser  IDENTIFIED BY &&randPassword  DEFAULT TABLESPACE &&inftbl  TEMPORARY TABLESPACE &&inftemptbl  PROFILE DEFAULT  ACCOUNT UNLOCK;

GRANT CONNECT TO &&randuser;
GRANT RESOURCE TO &&randuser;
ALTER USER &&randuser DEFAULT ROLE ALL;
GRANT UNLIMITED TABLESPACE TO &&randuser;

connect &&randuser/&&randPassword@&&dbinstance

CREATE TABLE DRUGKITS ( ID NUMBER, DESCRIPTION VARCHAR2(250 BYTE), LISTID NUMBER, SEQUENCENUMBER NUMBER ) TABLESPACE &&inftbl PCTUSED 40 PCTFREE 10 INITRANS 1 MAXTRANS 255 STORAGE ( INITIAL 128K NEXT 128K MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT ) LOGGING NOCACHE NOPARALLEL
/

CREATE TABLE RANDOMIZATION ( ID NUMBER, TYPE VARCHAR2(50 BYTE), INSTUDY NUMBER(1) ) TABLESPACE &&inftbl PCTUSED 40 PCTFREE 10 INITRANS 1 MAXTRANS 255 STORAGE ( INITIAL 128K NEXT 128K MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT ) LOGGING NOCACHE NOPARALLEL
/

CREATE TABLE SOURCELISTS ( ID NUMBER, LISTNAME VARCHAR2(500 BYTE), SEQUENCEUUID VARCHAR2(500 BYTE), MAX NUMBER, MAXALLOWED NUMBER, TYPEID NUMBER ) TABLESPACE &&inftbl PCTUSED 40 PCTFREE 10 INITRANS 1 MAXTRANS 255 STORAGE ( INITIAL 128K NEXT 128K MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT ) LOGGING NOCACHE NOPARALLEL
/

ALTER TABLE RANDOMIZATION ADD ( CONSTRAINT randpk PRIMARY KEY (ID))
/

ALTER TABLE SOURCELISTS ADD ( CONSTRAINT sourcepk PRIMARY KEY (ID), CONSTRAINT sourcefk FOREIGN KEY (TYPEID) REFERENCES RANDOMIZATION (ID))
/

ALTER TABLE DRUGKITS ADD ( CONSTRAINT drugpk PRIMARY KEY (ID), CONSTRAINT drugfk FOREIGN KEY (LISTID) REFERENCES SOURCELISTS (ID))
/

INSERT INTO RANDOMIZATION ( ID, TYPE, INSTUDY ) VALUES ( 1, 'SimpleCentral', 1); 
INSERT INTO RANDOMIZATION ( ID, TYPE, INSTUDY ) VALUES ( 2, 'CentralStratified', 0); 
INSERT INTO RANDOMIZATION ( ID, TYPE, INSTUDY ) VALUES ( 3, 'SimpleSite', 0); 
INSERT INTO RANDOMIZATION ( ID, TYPE, INSTUDY ) VALUES ( 4, 'StratifiedBySite', 0); 
COMMIT;

INSERT INTO Sourcelists VALUES (1,'SimpleCentral','5YGH57UO-943Q-85X1-647N-22V8G4449T21',300,300,1);
COMMIT;

--INSERT INTO DRUGKITS ( ID, DESCRIPTION, LISTID, SEQUENCENUMBER ) VALUES ( 1,'1|ABC-3TC',1,1);
insert into drugkits values(1, 'somerandcode' , 1, 1);
insert into drugkits values(2, 'somerandcode' , 1, 2);
insert into drugkits values(3, 'somerandcode' , 1, 3);
insert into drugkits values(4, 'somerandcode' , 1, 4);
insert into drugkits values(5, 'somerandcode' , 1, 5);
insert into drugkits values(6, 'somerandcode' , 1, 6);
insert into drugkits values(7, 'somerandcode' , 1, 7);
insert into drugkits values(8, 'somerandcode' , 1, 8);
insert into drugkits values(9, 'somerandcode' , 1, 9);
insert into drugkits values(10, 'somerandcode' , 1, 10);
insert into drugkits values(11, 'somerandcode' , 1, 11);
insert into drugkits values(12, 'somerandcode' , 1, 12);
insert into drugkits values(13, 'somerandcode' , 1, 13);
insert into drugkits values(14, 'somerandcode' , 1, 14);
insert into drugkits values(15, 'somerandcode' , 1, 15);
insert into drugkits values(16, 'somerandcode' , 1, 16);
insert into drugkits values(17, 'somerandcode' , 1, 17);
insert into drugkits values(18, 'somerandcode' , 1, 18);
insert into drugkits values(19, 'somerandcode' , 1, 19);
insert into drugkits values(20, 'somerandcode' , 1, 20);
insert into drugkits values(21, 'somerandcode' , 1, 21);
insert into drugkits values(22, 'somerandcode' , 1, 22);
insert into drugkits values(23, 'somerandcode' , 1, 23);
insert into drugkits values(24, 'somerandcode' , 1, 24);
insert into drugkits values(25, 'somerandcode' , 1, 25);
COMMIT;

grant select, insert, delete, update on DRUGKITS to &&informuid;

grant select, insert, delete, update on sourcelists to &&informuid;

grant select, insert, delete, update on randomization to &&informuid;

commit;

prompt > Randomization database setup successful.

spool off;
exit;