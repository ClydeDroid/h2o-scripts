require 'csv'
# Subject class stores all information contained in one row of the Subject file 
class Subject
  def initialize(site, subnum, infdate, rnd, rnddate)
    @site = site
    @subnum = subnum
    @infdate = infdate
    @rnd = rnd
    @rnddate = rnddate
    @complete, @compdate, @disdate, @disreason, @discomment = nil, nil, nil, nil, nil
    scrnnum
  end
  def scrnnum
    @scrnnum = @site + 'S' + @subnum
  end
  def completed(code, compdate, disdate)
    if code == '1' || '2' || '4'
      @complete = '1'
      @compdate = compdate
    elsif code == '0'
      @complete = '0'
      @disdate = disdate
    end
  end
  def disreason(reason, comment)
    @disreason = reason
    @discomment = comment
  end
  def out
    [ARGV[0], @site, @scrnnum, @subnum, @infdate, nil, @rnd, nil, nil, @rnddate, @disdate, @complete, @compdate, @disreason, @discomment, nil]
  end
end
# Visit class stores all information contained in one row of the Visit file 
class Visit
  def initialize(site, subnum, visitid, visit, date)
    @site = site
    @subnum = subnum
    @visitid = visitid
    @visit = visit
    @date = date
    scrnnum
  end
  def scrnnum
    @scrnnum = @site + 'S' + @subnum
  end
  def out
    [ARGV[0], @site, @scrnnum, @subnum, @visitid, @visit, @date, nil, nil]
  end
end
# SAE class stores all information contained in one row of the SAE file 
class SAE
  def initialize(aenum, site, subnum, date, term, relationship)
    @aenum = aenum
    @site = site
    @subnum = subnum
    @date = date[0..9] + ' ' + date[11..15]
    @term = term
    @relationship = relationship
    scrnnum
  end
  def scrnnum
    @scrnnum = @site + 'S' + @subnum
  end
  def out
    [ARGV[0], @site, @scrnnum, @subnum, @aenum, @date, @term, @relationship, nil]
  end
end
# Read files and create hashes matching IDs to Numbers
siteid, subid, visitid, subrnd, subinfdate = {}, {}, {}, {}, {}
CSV.foreach("IRV_CUR_SITE.csv", headers: true) do |row|
  siteid[row['SITEID']] = row['SITEMNEMONIC']
end
CSV.foreach("IRV_CUR_SUBJECT.csv", headers: true) do |row|
  subid[row['SUBJECTID']] = row['SUBJECTNUMBERSTR']
  subrnd[row['SUBJECTID']] = '1' if row['SUBJECTSTATETEXT'] == 'Randomized'
end
CSV.foreach("RD_EL.csv", headers: true) do |row|
  subrnd[row['SUBJECTID']] = '0' if row['ELYN'] == 'No'
end
CSV.foreach("IRV_STUDYVERSION_VISITS.csv", headers: true) do |row|
  visitid[row['VISITID']] = row['VISITNAME']
end
CSV.foreach("RD_IC.csv", headers: true) do |row|
  subinfdate[row['SUBJECTID']] = row['DSST1DTC_DTS']
end
# Read EOT and OLEOT files and create hashes matching Subject ID to Study Completion variables
subcomplete, subcompdate, subdisdate, subdisreason, subdiscomment = {}, {}, {}, {}, {}
CSV.foreach("RD_EOT.csv", headers: true) do |row|
  if row['EOTEVL'] == 'Yes'
    subcomplete[row['SUBJECTID']] = row['EOT175_C']
    subcompdate[row['SUBJECTID']] = row['EOTDTC_DTS']
  elsif row['EOTEVL'] == 'No'
    subcomplete[row['SUBJECTID']] = '0' 
    subdisdate[row['SUBJECTID']] = row['EOTDTC_DTS']
    subdisreason[row['SUBJECTID']] = row['EOTR']
    subdiscomment[row['SUBJECTID']] = row['EOTSPE']
  end
end
CSV.foreach("RD_OLEOT.csv", headers: true) do |row|
  if row['OLEOTEVL'] == 'Yes'
    subcomplete[row['SUBJECTID']] = '1' 
    subcompdate[row['SUBJECTID']] = row['OLEOTDTC_DTS']
  elsif row['OLEOTEVL'] == 'No'
    subcomplete[row['SUBJECTID']] = '0' if row['OLEOTEVL'] == 'No'
    subdisdate[row['SUBJECTID']] = row['OLEOTDTC_DTS']
    subdisreason[row['SUBJECTID']] = row['OLEOTR']
    subdiscomment[row['SUBJECTID']] = row['OLEOTSPE']
  end
end
# Read DOV file, create hashes matching Subject ID to Informed Consent Date and Randomization Date, and create Visit instances, storing them in an array
subrnddate = {}
visits = []
CSV.foreach("RD_DOV.csv", headers: true) do |row|
  subrnddate[row['SUBJECTID']] = row['DOVDTC_DTS'] if row['VISITMNEMONIC'] == 'Baseline' && subrnd[row['SUBJECTID']] == '1'
  visits << Visit.new(siteid[row['SITEID']], subid[row['SUBJECTID']], row['VISITID'], visitid[row['VISITID']], row['DOVDTC_DTS'])
end
# Read subjects file and create Subject instances, storing them in an array
subjects = []
count = 0
CSV.foreach("IRV_CUR_SUBJECT.csv", headers: true) do |row|
  subjects << Subject.new(siteid[row['SITEID']], row['SUBJECTNUMBERSTR'], subinfdate[row['SUBJECTID']], subrnd[row['SUBJECTID']], subrnddate[row['SUBJECTID']])
  subjects[count].completed(subcomplete[row['SUBJECTID']], subcompdate[row['SUBJECTID']], subdisdate[row['SUBJECTID']]) if subcomplete[row['SUBJECTID']]
  subjects[count].disreason(subdisreason[row['SUBJECTID']], subdiscomment[row['SUBJECTID']]) if subdisreason[row['SUBJECTID']]
  count += 1
end
# Read AE file and create SAE instances, storing them in an array
saes = []
CSV.foreach("RD_AE.csv", headers: true) do |row|
  saes << SAE.new(row['FORMIDX'], siteid[row['SITEID']], subid[row['SUBJECTID']], row['AESTDTC_DTS'], row['AETERM'], row['AEREL']) if row['AESER'] == 'Yes'
end
# Write Subjects file
CSV.open("out/#{ARGV[0]}_SUBJECTS.csv", 'w', col_sep: '|') do |ctms_rows|
  subjects.each do |subject|
    ctms_rows << subject.out
  end
end
# Write Visits file
CSV.open("out/#{ARGV[0]}_VISITS.csv", 'w', col_sep: '|') do |ctms_rows|
  visits.each do |visit|
    ctms_rows << visit.out
  end
end
# Write SAE file
CSV.open("out/#{ARGV[0]}_SAE.csv", 'w', col_sep: '|') do |ctms_rows|
  saes.each do |sae|
    ctms_rows << sae.out
  end
end