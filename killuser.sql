set verify off
define dbauser = &1
declare
   str     varchar2(100);
begin
   for SES in (select sid,serial# from v$session where username='&&dbauser') loop
           str := 'alter system kill session '||chr(39)||SES.sid||','||SES.serial#||chr(39);
           execute immediate str;
   end loop;
end;
/
drop user &&dbauser cascade;
exit;