# H2O Scripts

These are just a couple of scripts I've written while at H2O as a part of my job duties and to make my life a little easier.

### devtrial.cmd

This little command line program makes setting up InForm testing environments as easy as one command, `devtrial setup trialname`.

### randsetup.sql

This script was written to set up a database and insert randomization codes for a clinical trial I worked on. The codes would be pulled from the db every time a patient was randomized.

### killuser.sql

This script was used by devtrial.cmd to disconnect all users from a test trial's db before dropping the schema.

### CTMSerator.rb

This Ruby script took raw extracts of clinical trial data and converted the needed data into CSV format for an external system to use.

### Daily_Transfer.bat

This batch file took the CSV files output by CTMSerator.rb, zipped them up, and transferred them to an sFTP location using WinSCP.